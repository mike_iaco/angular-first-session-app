export interface Monsters {
  img: string;
  name: string;
  health: number;
  loots: string[];
  experience: number;
  locationDetail: Location;
}

export interface Location {
    weather: string;
    cave: string;
    secondCave: boolean;
}
