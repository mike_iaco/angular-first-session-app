import { Component, OnInit } from '@angular/core';
import { Monsters } from './models/IContent';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss'],
})
export class ContentComponent implements OnInit {
  public monsters: Monsters[];
  constructor() {
    this.monsters = [
      {
        img: '../../../assets/img/Spellstealer.gif',
        name: 'Spellstealer',
        health: 4000,
        loots: ['Gold', 'Boots of Haste', 'Meat'],
        experience: 150000,
        locationDetail: {
          weather: 'Desert',
          cave: 'Ankrahmun',
          secondCave: true,
        }
      },
      {
        img: '../../../assets/img/Ferumbras.gif',
        name: 'Ferumbras',
        health: 10000,
        loots: ['Cristal Coin', 'Ham', 'Health Potion', 'Mastermind Shield'],
        experience: 120000,
        locationDetail: {
          weather: 'Fire',
          cave: 'Ferumbras Tower',
          secondCave: false,
      },
      },
      {
        img: '../../../assets/img/Gravedigger.gif',
        name: 'Gravedigger',
        health: 80000,
        loots: ['Fingers', 'Platinum Coin', 'Fish'],
        experience: 55000,
        locationDetail: {
          weather: 'Water',
          cave: 'AquaZone',
          secondCave: false,
      }
      },
      {
        img: '../../../assets/img/DepolarizedCrackler.gif',
        name: 'Depolarized Crackler',
        health: 25000,
        loots: ['Gold', 'Golden Boots', 'Diamant Shield'],
        experience: 92100,
        locationDetail: {
          weather: 'Electric',
          cave: 'ElementalZone',
          secondCave: false,
      }
      },
      {
        img: '../../../assets/img/Terrorsleep.gif',
        name: 'Terrorsleep',
        health: 15000,
        loots: ['Gold', 'Waterwalking Boots', 'Great Shield'],
        experience: 99900,
        locationDetail: {
          weather: 'Water',
          cave: 'AquaZone',
          secondCave: false,
      }
      },
    ];
  }

  ngOnInit(): void {}
}
